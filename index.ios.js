'use strict';
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    Animated,
    Image,
    TouchableHighlight
} from 'react-native';

class CSSAnimation extends Component {
    constructor(props:any) {
        super(props);
        this.state = {
            rotateValue: new Animated.Value(0),
        };
    }

    render() {
        var animStyle = {
            width: 160,
            height: 61,
            transform: [
                {
                    translateY: 70 
                },
                {
                    translateX: 80
                },
                {
                    rotateZ: this.state.rotateValue.interpolate({
                        inputRange: [0, 1],
                        outputRange: ['0deg', '360deg']
                    })
                }
            ],
            backgroundColor: 'transparent'
        };

        var bottomStyle = {
            width: 142,
            height: 110,
            backgroundColor: 'transparent',
            transform: [
                {
                    translateY: 200
                }
            ]
        };

        return (
            <View style={styles.container}>
                <Image
                    source={require('./img/bottom.png')}
                    style={bottomStyle}
                />
                <Animated.View                         // Base: Image, Text, View
                    style={animStyle}
                >
                    <Image
                        source={require('./img/dabi.png')}
                        style={{
                            width: 160,
                            height: 61,
                        }}
                    />
                </Animated.View>
                <TouchableHighlight onPress={this._rotate.bind(this)}>
                    <Text>Rotate</Text>
                </TouchableHighlight>
            </View>
        );
    }

    componentDidMount() {
        this._rotate();
    }

    _rotate() {
        this.state.rotateValue.setValue(1);
        Animated.timing(this.state.rotateValue, {
            toValue: 0,
            duration: 1000,
        }).start();                                // Start the animation
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});

AppRegistry.registerComponent('CSSAnimation', () => CSSAnimation);
